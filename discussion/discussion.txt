// aggregation in mongodb and query case studies

db.fruits.insertMany([
		{
			name : "Apple",
			color : "Red",
			stock : 20,
			price: 40,
			supplier_id : 1,
			onSale : true,
			origin: [ "Philippines", "US" ]
		},

		{
			name : "Banana",
			color : "Yellow",
			stock : 15,
			price: 20,
			supplier_id : 2,
			onSale : true,
			origin: [ "Philippines", "Ecuador" ]
		},

		{
			name : "Kiwi",
			color : "Green",
			stock : 25,
			price: 50,
			supplier_id : 1,
			onSale : true,
			origin: [ "US", "China" ]
		},

		{
			name : "Mango",
			color : "Yellow",
			stock : 10,
			price: 120,
			supplier_id : 2,
			onSale : false,
			origin: [ "Philippines", "India" ]
		}     	
])


//codes
db.fruits.find()
db.fruits.aggregate({$match:{color:"Yellow"}},{$group:{_id:"$color",total:{$sum:"$stock"}}})

//1 stage only
.count() - for counting an object or specific element
db.fruits.count(); deprecated
db.fruits.countDocuments();

//fruits is column name
db.fruits.aggregate([
{$count: "fruits"}
])


db.fruits.aggregate(
    {$match:{onSale:true}}, {$count:"fruitsOnSale"}
)

//match and group
db.fruits.aggregate(
    {$match:{onSale:true}}, {$group:{_id:"$supplier_id", total:{$sum: "$stock"}}}
)
//multiple column
db.fruits.aggregate(
    {$match:{onSale:true}}, {$group:{_id:{supplier_id:"$supplier_id",name:"$name"}, total:{$sum: "$stock"}}}
)

another example

db.fruits.aggregate(
    {$match:{onSale:true}}, {$group:{_id:"$supplier_id", totalPrice:{$sum: "$price"}}}
)


//third phase optional $project phase is responsible for excluding certain fields that do not need to show up in the final results
db.fruits.aggregate(
    {$match:{onSale:true}}, 
    {$group:{_id:"$supplier_id", totalPrice:{$sum: "$price"}, totalStocks:{$sum:"$stock"}}
    
    }, {$project:{totalPrice:1, _id:0}}
)

//4th phase sort 1 is ascending
db.fruits.aggregate(
    {$match:{onSale:true}}, 
    {$group:{_id:"$supplier_id", totalPrice:{$sum: "$price"}, totalStocks:{$sum:"$stock"}}
    
    }, {$project:{totalPrice:1, _id:0}},
    {$sort:{totalPrice:1}}
)


// $unwind operator - responsible for deconstructuring
// an array and using them as the unique identifier

db.fruits.aggregate(
    {$undwind:"$origin"}
)

//order matters in pipeline


db.fruits.aggregate([
        { $unwind: "$origin" },
        { $group : { _id : "$origin", kinds : { $sum : 1 } } }
]);


db.fruits.aggregate([
        { $unwind: "$origin" },
        {$match: {origin:"Philippines"}},
        
]);

//array ung pipeline

/*
// For Activity
$avg, $min, $max


https://www.mongodb.com/docs/manual/reference/operator/aggregation/group/#mongodb-pipeline-pipe.-group

*/